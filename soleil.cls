\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{soleilbeamer}[2021/09/09 Presentation SOLEIL]
\RequirePackage{tikz}
\RequirePackage{amssymb}
\RequirePackage{pifont}
\RequirePackage[load-configurations = abbreviations]{siunitx}
\RequirePackage{pgfplotstable} % Generates table from .csv
\RequirePackage{booktabs}
\RequirePackage{pgf}% or \RequirePackage{tikz}
\pgfplotsset{compat=1.17}
\RequirePackage{lipsum}
% Hot fix for enumitem and fontspec, won't actually affect font sizes
\renewcommand\normalsize{\fontsize{10pt}{12pt}\selectfont}
\newcommand\scriptsize{\fontsize{10pt}{12pt}\selectfont}
\newcommand\footnotesize{\fontsize{10pt}{12pt}\selectfont}
\newcommand\tiny{\fontsize{10pt}{12pt}\selectfont}
\newcommand\large{\fontsize{10pt}{12pt}\selectfont}
\newcommand\Large{\fontsize{10pt}{12pt}\selectfont}
\newcommand\LARGE{\fontsize{10pt}{12pt}\selectfont}
\newcommand\huge{\fontsize{10pt}{12pt}\selectfont}
\newcommand\Huge{\fontsize{10pt}{12pt}\selectfont}
\newcommand\small{\fontsize{10pt}{12pt}\selectfont}
\RequirePackage[loadonly]{enumitem}
\RequirePackage{fontspec}
\setsansfont{Arial}
\renewcommand{\familydefault}{\ssdefault}
\sisetup{locale = FR,
detect-all,
}
\sisetup{
range-phrase=--,     % Utilise le tiret court pour dire "de... à"
range-units=single,  % Cache l'unité sur la première borne
}
\sisetup{inter-unit-product=\ensuremath{\cdot}}


%%%%%% Définition des options et variables de la classe SOLEIL 
\newcommand{\aspect}{4-3}
\newcommand{\aspectbeamer}{43}
\newcommand{\upgrade}{}
\newcommand{\firstaerial}{b}
\newcommand{\backgroundbasename}{Modele-Presentation-PPT-SOLEIL-}
\newcommand{\marginxgarde}{-28}
\newcommand{\marginygarde}{-164}
\newcommand{\marginxtitle}{100}
\newcommand{\marginytitle}{-110}
\newcommand{\titlelength}{0.6}
\newcommand{\maxbackground}{5}
\newcommand{\startbackground}{3}
\newcommand{\frametitlewidth}{0.76\paperwidth}
\DeclareOption{aspect43}{
    \renewcommand{\aspect}{4-3}
    \renewcommand{\aspectbeamer}{43}
}
\DeclareOption{aspect169}{
    \renewcommand{\aspect}{16-9}
    \renewcommand{\aspectbeamer}{169}
    \renewcommand{\marginygarde}{-154}
    \renewcommand{\titlelength}{0.68}
    \renewcommand{\frametitlewidth}{0.8\paperwidth}
}
\DeclareOption{upgrade}{
    \renewcommand{\upgrade}{UPGRADE-}
    \renewcommand{\firstaerial}{a}
    \renewcommand{\maxbackground}{6}
    \renewcommand{\startbackground}{2}
}

\DeclareOption{photo}{\renewcommand{\firstaerial}{a}}
\ProcessOptions\relax

\LoadClass[aspectratio=\aspectbeamer]{beamer}
\DeclareOptionBeamer{compress}{\beamer@compresstrue}
\ProcessOptionsBeamer

\mode<presentation>

% Pour avoir un menu naviguable au dessus des titres de slide : décommenter la ligne suivante
% et commenter le vspace en tete du header 
%\useoutertheme[footline=authortitle]{miniframes}
\usecolortheme{whale}
\usecolortheme{orchid}
\useinnertheme{rectangles}

\setbeamerfont{block title}{size={}}

\mode
<all>

\newlist{todolist}{itemize}{2}
\setlist[todolist]{label=$\square$}
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%
\newcommand{\done}{\rlap{$\square$}{\raisebox{2pt}{\large\hspace{1pt}\cmark}\hspace*{-1pt}}}
\newcommand{\wontfix}{\rlap{$\square$}{\large\hspace{1pt}\xmark}\hspace*{1pt}}

%%%%%%% Definition des couleurs du thème

\mode<presentation>

\definecolor{bleuSOLEIL}{HTML}{004C9C} % bleu
\definecolor{jauneSOLEIL}{HTML}{FBBA06} % jaune


\setbeamercolor*{normal text}{fg=bleuSOLEIL,bg=white}
\setbeamercolor*{alerted text}{fg=bleuSOLEIL!10!red, bg=gray!50}
\setbeamercolor*{example text}{fg=bleuSOLEIL!50!green, bg=gray!25}
\setbeamercolor*{structure}{fg=bleuSOLEIL, bg=gray!10}

\setbeamercolor*{block body}{parent=structure}
\setbeamercolor*{block body alerted}{parent = alerted text}
\setbeamercolor*{block body example}{parent = example text}
\setbeamercolor*{block title}{parent=structure, bg=gray!20!structure.bg}
\setbeamercolor*{block title alerted}{parent=alerted text, bg=gray!20!alerted text.bg}
\setbeamercolor*{block title example}{parent=example text, bg=gray!20!example text.bg}

\providecommand*{\beamer@bftext@only}{%
  \relax
  \ifmmode
    \expandafter\beamer@bftext@warning
  \else
    \expandafter\bfseries
  \fi
}
\providecommand*{\beamer@bftext@warning}{%
  \ClassWarning{beamer}
    {Cannot use bold for alerted text in math mode}%
}


\setbeamercolor{normal text}{fg=bleuSOLEIL}
\setbeamercolor{frametitle}{fg=bleuSOLEIL}

\setbeamerfont{alerted text}{series=\beamer@bftext@only}

\setbeamercolor{palette primary}{fg=bleuSOLEIL,bg=}
\setbeamercolor{palette secondary}{fg=bleuSOLEIL,bg=}
\setbeamercolor{palette tertiary}{fg=bleuSOLEIL,bg=}
\setbeamercolor{palette quaternary}{fg=bleuSOLEIL,bg=white}

\setbeamercolor{sidebar}{bg=black!20}

\setbeamercolor{palette sidebar primary}{fg=black}
\setbeamercolor{palette sidebar secondary}{fg=black}
\setbeamercolor{palette sidebar tertiary}{fg=black}
\setbeamercolor{palette sidebar quaternary}{fg=black}

\setbeamercolor{item projected}{fg=black,bg=black!20}

\setbeamertemplate{itemize items}[circle]

\setbeamercolor*{titlelike}{parent=structure}

%%%%%%%%%%%%%  définition de la slide de garde

\pgfdeclareimage[width=\paperwidth]{titlebackground}{images/\backgroundbasename\upgrade\aspect-Fond\firstaerial}
\setbeamertemplate{title page}{
    
        \begin{picture}(0,0)
            %\put(-30,-163){%
            \put(\marginxgarde,\marginygarde){%
                \pgfuseimage{titlebackground}
            }
            %\put(100,-110){%
            \put(\marginxtitle,\marginytitle){%
	            \noindent\fcolorbox{gray}{gray}{%
	                \begin{minipage}{\titlelength\paperwidth}
	                    \usebeamerfont{title}{\color{white}\inserttitle}
		            	\textcolor{jauneSOLEIL}{\hrule}
	                \end{minipage}
	                }
            }
            \end{picture}
 }
    
    \makeatletter

%%%%%%%%%%%%% Définition des images de fond cyclant entre les fonds soleil :
\mode
<all>
%\makeatletter
\newcount\mycount
\global\mycount=\startbackground\relax%
\newcommand*{\randimg}{%
  \global\advance\mycount by 1\relax%
  \ifnum\mycount>\maxbackground\relax%
    \global\mycount=\startbackground\relax%
  \fi%
  \includegraphics[width=\paperwidth,height=\paperheight]%
   {images/\backgroundbasename\upgrade\aspect-Fond\@alph{\mycount}}  
    }

\makeatother

\setbeamertemplate{background}{%
  \randimg%
}

%%%%%%%%%%%%%%%%% Customisation du footer
\makeatother
\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
	  \begin{beamercolorbox}[wd=.2\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}%
	    \usebeamerfont{author in head/foot}\hspace*{6ex}\insertshortauthor
	  \end{beamercolorbox}%
	  \begin{beamercolorbox}[wd=.8\paperwidth,ht=2.25ex,dp=1ex,right]{title in head/foot}%
	    \usebeamerfont{title in head/foot}\insertshorttitle\hspace*{3em}
	    \insertframenumber{} / \inserttotalframenumber\hspace*{1ex}
	  \end{beamercolorbox}%
	}
  \vskip0pt%
}
\makeatletter

%%%%%%%%%%%%%% Customisation du header
\setbeamertemplate{frametitle}{
	\vspace*{1cm} % commenter cette ligne si on active les miniframes
    \hspace*{1cm} %%%%%%%%%%%%% For example insert shift to right
    \begin{beamercolorbox}[sep=-0.3cm,right,wd=\frametitlewidth]{frametitle}%wd=\the\@tempdima]{frametitle}
        \usebeamerfont{frametitle}%
        \vbox{}\vskip-10ex%
        \if@tempswa\else\csname beamer@ftecenter\endcsname\fi%
        \strut\insertframetitle\strut%
        {%
            \ifx\insertframesubtitle\@empty%
            \else%
            {\usebeamerfont{framesubtitle}\usebeamercolor[fg]{framesubtitle}\par\insertframesubtitle\strut}%
            \fi
        }%
       \textcolor{jauneSOLEIL}{\hrule}
        \vskip-1ex%
        \if@tempswa\else\vskip-.3cm\fi% set inside beamercolorbox... evil here...

    \end{beamercolorbox}%
}
\makeatother

%%%%%%%%%%%% Ajout de TOC automatique en debut de section
\AtBeginSection[]
{{%\removepagenumbers
  \begin{frame}[noframenumbering,plain]{\iflanguage{french}{Table des matières}{Table of contents}}
    \tableofcontents[currentsection]
  \end{frame}}
}
