Trames LaTeX beamer pour SOLEIL
===============================

Essai de trames respectant la charte graphique de SOLEIL pour 
la composition de présentation avec beamer LaTeX.


Installation
------------
Cloner ce dépôt et éditer (et renommer) le fichier example.tex 

example.pdf ne sert qu'à titre d'illustration du contenu d'exemple.tex 
Il peut être effacé après renommage d'example.tex

Licence
-------
Voir fichier de licence. Contenu sous licence GPL
